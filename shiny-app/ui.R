#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

my_packages <- c("shiny", "plyr", "dplyr", "ggplot2",
                 "openxlsx","highcharter","htmlwidgets",
                 "gdata", "shinydashboard", "readxl",
                 "tidyverse", "tidytext","tokenizers", "tm",
                 "wordcloud","wordcloud2","reshape2",
                 "ggplot2", "lubridate","numform")
not_installed <- my_packages[!(my_packages %in% installed.packages()[ , "Package"])]
if(length(not_installed)) install.packages(not_installed)
lapply(my_packages,library, character.only = TRUE)

#old <- getOption("defaultPackages")
#options(defaultPackages = c(old, "tidyverse"))
#datostodo <- readxl::read_excel("/home/liacco/NextCloud/fcosilva/TRABAJO/SENESCYT/DDI/DATA-LU/20112019_Estado-de-Proyectos-Evaluaciones.xlsx", sheet = 2)

#
# Define UI for application
#
shinyUI(
  fluidPage(
   dashboardPage(skin = "blue", 
                 dashboardHeader(title = "Compras Públicas de Software - Ecuador (2009 - 2018)", titleWidth = "95%"#,
                 #dropdownMenu(type = "tasks")
                 ),
                 dashboardSidebar(collapsed = FALSE,
                   sidebarMenu(
                     menuItem("Dashboard", tabName = "dashboard", icon = icon("dashboard"),selected = TRUE),
                     menuItem("Proveedores", tabName = "proveedores", icon = icon("chalkboard-teacher")),
                     menuItem("About", tabName = "about", icon = icon("th"))
                     )
                   ),
                 dashboardBody(tags$head(
                   tags$link(rel = "stylesheet", type = "text/css", href = "custom.css")),
                   tabItems(
                     tabItem(tabName = "dashboard",
                        #box(width = 12,
                             fluidRow(#column(4, #offset = 0,
                                      #       box(width = 12, collapsible = TRUE,
                                      #           dateRangeInput("rangofecha", "Seleccione rango",
                                      #                          start = "2007-01-01",
                                      #                          end = "2018-12-31",
                                      #                          #min ="2007-01-01",
                                      #                          #max ="2018-12-31",
                                      #                          autoclose = TRUE,
                                      #                          language = "ES",
                                      #                          separator = " - "))
                                      #       ),
                                      #column(4, #offset = 1,
                                      #       box(width = 12, collapsible = TRUE,
                                      #           selectInput("in_provincia_p", "Provincia",choices = c("todos",l_provincia_p))) #end Box
                                      #), #end column
                                      #column(4, #offset = 1,
                                      #       box(width = 12, collapsible = TRUE,
                                      #         selectInput("in_anio", "Año de compra",choices = c("todos",l_anio))) #end Box
                                      # ), #end column
                                      
                                      column(12,#offset = 0, 
                                              box(width = 12, collapsible = TRUE,
                                                selectInput("in_cpcn8", "CPC Nivel 8:", 
                                                            choice = c("Todos" = "todos", setNames(as.character(l_cpcn8$n8),as.character(l_cpcn8$descripcion_n8))),
                                                            selected = "Todos") 
                                                ) #end Box
                                      ) #end column
                                      #h4("Titulo H4"),
                                      #a("MIGRALAB.EC", href = "https://www.migralab.ec")
                                    ), # End fluidRow,
                             fluidRow(
                                      column(6, box(width = 12, #h4("Nube de palabras - compras 209-2018", align = "center", style = "bold"),
                                             plotOutput("hc3", "100%")),
                                             box(width = 12,highchartOutput("hc1")
                                                  )
                                             ) ,
                                      column(6, #offset = 0,
                                             box(width = 12, 
                                             highchartOutput("hc2", height = 843)      
                                             ) # end box
                                             ) # End column,
                                      #column(6, #offset = 0,
                                      #       box(width = 9, highchartOutput("hc2")) # end box
                                      #       ),
                                      #column(6, #offset = 0,
                                             #box(width = 12, 
                                                 #highchartOutput("hc1")
                                             #) # end box
                                      #       ) # End column
                                      ) # End fluidRow
                        #) # End Box
                        
                        #fluidRow(
                        #  column(12
                                 #highchartOutput("hc2")
                        #         ) # End column
                        #)
                             ) , # End tabItem
                     tabItem(tabName ="proveedores",
                             
                             fluidRow(
                               column(12, #offset = 0,
                                      box(width = 12, 
                                          highchartOutput("hc4")      
                                      ) # end box
                               ) # End column,                             
                             ), # End fluidRow
                             fluidRow(
                               column(6,
                                      box(width = 12,
                                          highchartOutput("hc5")
                                          )
                                      ),
                               column(6,
                                      box(width = 12,
                                          highchartOutput("hc6")
                                          )
                                     ) 
                             )
                     ),
                     tabItem(tabName = "about",
                             h4("Contenido de TabName About")
                             )
                     ) #end TabItems
                   ) #end Body
          ) #end DashboardPage
) #end FluidPage
)
   
     
   
   # Application title
   #titlePanel("Programa ejemplo - datos DDI"),
   
   # Sidebar with a slider input for number of bins 
   #sidebarLayout(
  #  sidebarPanel(
  #      selectInput("anio", "Año de convocatoria",choices = aniose), 
  #      sliderInput("bins",
  #                   "Number of bins:",
  #                   min = 1,
  #                   max = 50,
  #                   value = 30)
  #    ),
      
      # Show a plot of the generated distribution
  #    mainPanel(
  #      highchartOutput("distPlot") 
        #plotOutput("distPlot")